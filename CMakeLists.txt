cmake_minimum_required (VERSION 2.8)
project (Heat)
enable_language(C)

option(HEAT_USE_MPI "Build MPI executable" OFF)

# libheat
include_directories(include)
add_subdirectory(lib)

# heat_seq exe
set(HEAT_SOURCES heat_seq.c mat_utils.c)
add_executable(heat_seq ${HEAT_SOURCES} mat_utils.h)
target_link_libraries(heat_seq heat m)
# install heat_seq
install(TARGETS heat_seq DESTINATION bin)

# heat_par exe - depends on MPI
if (HEAT_USE_MPI)

    find_package(MPI)
    if(MPI_FOUND)
	include_directories(${MPI_INCLUDE_PATH})
	add_executable(heat_par heat_par.c mat_utils.c mat_utils.h)
	target_link_libraries(heat_par ${MPI_C_LIBRARIES} heat m)
	install(TARGETS heat_par DESTINATION bin)
    else(MPI_FOUND)
	message(WARNING ”MPI not found” )
    endif(MPI_FOUND)

endif(HEAT_USE_MPI)

# uninstall target
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_modules/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)
add_custom_target(uninstall
  "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_modules/cmake_uninstall.cmake")

# tests
enable_testing()
include(CTest)
if(BUILD_TESTING)
  # ... CMake code to create tests ...
  add_test(heat_seq_usage ./heat_seq)
  set_tests_properties(heat_seq_usage PROPERTIES PASS_REGULAR_EXPRESSION "Usage*")
  add_test(heat_seq_err_10 ./heat_seq 10 10 1 0)
  set_tests_properties(heat_seq_err_10 PROPERTIES PASS_REGULAR_EXPRESSION "1.732*")
  if(HEAT_USE_MPI AND MPI_FOUND)
    add_test(heat_par_4 ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} 4 ./heat_par 10 10 200 2 2 0)
  endif(HEAT_USE_MPI AND MPI_FOUND)
endif()